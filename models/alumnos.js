import conexion from "./conexion.js";

const alumnosDB = {};

alumnosDB.insertar = async (alumno) => {
  return new Promise((resolve, reject) => {
    const sqlQuery = "INSERT INTO alumnos SET ?";
    conexion.query(sqlQuery, alumno, (err, result) => {
      if (err) {
        console.error("Surgió un error al insertar: ", err.message);
        return reject(err);
      }
      const alumnoInsertado = {id: result.insertId};
      resolve(alumnoInsertado);
    });
  });
}

alumnosDB.mostrarTodos = async () =>{
  return new Promise((resolve, reject) => {
    const sqlQuery = "SELECT * FROM alumnos";
    conexion.query(sqlQuery, null, (err, result) => {
      if (err) {
        console.error("Surgió un error al mostrar todos: ", err.message);
        return reject(err);
      }
      return resolve(result);
    });
  });
}

export default alumnosDB;
