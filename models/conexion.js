
import mysql from 'mysql2';

const conexion = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: 'cisco123',
    database:'sistemas',
});

conexion.connect( (err)  =>{
    if(err){
        console.log({err});
        return;
    }
    console.log('Se ha abierto la conexion con exito');
});

export default conexion;