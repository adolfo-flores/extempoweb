import express from "express";

export const router = express.Router();

export default { router };

// Practica alumnos
import alumnosDB from "../models/alumnos.js";

router.get("/", async (req, res) => {
  const rows = await alumnosDB.mostrarTodos();
  res.render("partials/practicas/alumnos", {regs: rows});
});

router.post("/", async (req, res) => {
  try {
    const alumnoData = {
      matricula: req.body.matricula,
      nombre: req.body.nombre,
      domicilio: req.body.domicilio,
      sexo: req.body.sexo,
      especialidad: req.body.especialidad,
    };
    const result = await alumnosDB.insertar(alumnoData);
    console.log({ result });
  } catch (error) {
    console.error(`Sucedio un error al intentar insertar alumno desde router/index.js ${error}`);
    res.status(400).send("Sucedio un error: " + error);
  }
  res.redirect('/');
});
